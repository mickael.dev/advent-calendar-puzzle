import { useEffect, useState, ReactNode } from "react";
import styles from '../styles/Home.module.css';
import Image from "next/image";
import swag from '../public/img/guirlande.png'
// import { Button }  from "./utilities/Button";

export interface Dates {
    date: string,
    hours: string,
    minutes: string,
    seconds: string
}

interface TCountDown {
    countDown?: number | any
    // any props that come into the component
}

const DateCalculation = ({countDown}: TCountDown) => {
    const [numberOfDays, setNumberOfDays] = useState<Dates["date"]>("0");
    const [numberOfHours, setNumberOfHours] = useState<Dates["hours"]>("0");
    const [numberOfMinutes, setNumberOfMinutes] = useState<Dates["minutes"]>("0");
    const [numberOfSeconds, setNumberOfSeconds] = useState<Dates["seconds"]>("0");

    useEffect(() => {
        const convertMsInDate = (countDown: number) => {
          let seconds = Math.floor((countDown / 1000) % 60);
          let minutes = Math.floor((countDown / (1000 * 60)) % 60);
          let hours = Math.floor((countDown / (1000 * 60 * 60)) % 24);
          let days = Math.floor(countDown / (1000 * 3600 * 24));
          console.log(days);
          return (
            setNumberOfDays(days.toString()),
            setNumberOfHours(hours.toString()),
            setNumberOfMinutes(minutes.toString()),
            setNumberOfSeconds(seconds.toString())
          );
        }
        convertMsInDate(countDown);
    }, [numberOfSeconds, countDown])
    
    return (
        <div className={styles.adventCalendar__dateCases}>
            <div className={styles.adventCalendar__dateCase}>
                {numberOfDays}
            </div>
            <div className={styles.adventCalendar__dateLetters}>
                d
            </div>
            <div className={styles.adventCalendar__dateCase}>
                {numberOfHours}
            </div>
            <div className={styles.adventCalendar__dateLetters}>
                h
            </div>
            <div className={styles.adventCalendar__dateCase}>
                {numberOfMinutes}
            </div>
            <div className={styles.adventCalendar__dateLetters}>
                m
            </div>
            <div className={styles.adventCalendar__dateCase}>
                {numberOfSeconds}
            </div>
            <div className={styles.adventCalendar__dateLetters}>
                s
            </div>
            <Image  
                src={swag} 
                width={400} 
                // // height={}
                alt="Swag picture"
                className={styles.adventCalendar__swag}
            />
        </div>
    );
    
    
}

export default DateCalculation;