import Link from "next/link";

interface TProps {
    href: string;
    text: string;
}

export const Button = ({ href, text }: TProps) => {
  return (
    <Link className="button max-w-max" href={href}>
      {text}
    </Link>
  );
};
