
interface TDatePosition {
  date: string;
  imageUrl: string;
}

interface TDatesPositions extends Array<TDatePosition>{}


const DatesPositions: TDatesPositions = [
  {
    date: "01/11/2023",
    imageUrl: "/img/advent_img/day1.jpg",
  },
  {
    date: "02/11/2023",
    imageUrl: "/img/advent_img/day2.jpg",
  },
  {
    date: "03/11/2023",
    imageUrl: "/img/advent_img/day3.jpg",
  },
  {
    date: "04/11/2023",
    imageUrl: "/img/advent_img/day4.jpg",
  },
  {
    date: "05/11/2023",
    imageUrl: "/img/advent_img/day5.jpg",
  },
  {
    date: "06/11/2023",
    imageUrl: "/img/advent_img/day6.jpg",
  },
  {
    date: "07/11/2023",
    imageUrl: "/img/advent_img/day7.jpg",
  },
  {
    date: "08/11/2023",
    imageUrl: "/img/advent_img/day8.jpg",
  },
  {
    date: "09/11/2023",
    imageUrl: "/img/advent_img/day9.jpg",
  },
  {
    date: "10/11/2023",
    imageUrl: "/img/advent_img/day10.jpg",
  },
  {
    date: "11/11/2023",
    imageUrl: "/img/advent_img/day11.jpg",
  },
  {
    date: "12/11/2023",
    imageUrl: "/img/advent_img/day12.jpg",
  },
  {
    date: "13/11/2023",
    imageUrl: "/img/advent_img/day13.jpg",
  },
  {
    date: "14/11/2023",
    imageUrl: "/img/advent_img/day14.jpg",
  },
  {
    date: "15/11/2023",
    imageUrl: "/img/advent_img/day15.jpg",
  },
  {
    date: "16/11/2023",
    imageUrl: "/img/advent_img/day16.jpg",
  },
  {
    date: "17/11/2023",
    imageUrl: "/img/advent_img/day17.jpg",
  },
  {
    date: "18/11/2023",
    imageUrl: "/img/advent_img/day18.jpg",
  },
  {
    date: "19/11/2023",
    imageUrl: "/img/advent_img/day19.jpg",
  },
  {
    date: "20/11/2023",
    imageUrl: "/img/advent_img/day20.jpg",
  },
  {
    date: "21/11/2023",
    imageUrl: "/img/advent_img/day21.jpg",
  },
  {
    date: "22/11/2023",
    imageUrl: "/img/advent_img/day22.jpg",
  },
  {
    date: "23/11/2023",
    imageUrl: "/img/advent_img/day23.jpg",
  },
  {
    date: "24/11/2023",
    imageUrl: "/img/advent_img/day24.jpg",
  },
];

export default DatesPositions;