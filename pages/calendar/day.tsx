import styles from "../../styles/day.module.css";
import { useRouter } from 'next/router'
import { JigsawPuzzle } from 'react-jigsaw-puzzle/lib'
import 'react-jigsaw-puzzle/lib/jigsaw-puzzle.css'
import { useCallback, useEffect, useState } from "react";
import Particles from "react-particles";
import type { Container, Engine } from "tsparticles-engine";
import { loadFull } from "tsparticles";
import Image from "next/image";
import swag from '../../public/img/guirlande.png'
import Dates from "../../components/daysPositions";

const Day = () => {
  let router = useRouter();
  const [success, setSuccess] = useState<boolean>(false);
  const [dateInfos, setDateInfo] = useState<{
    date: string;
    imageUrl: string;
  }>();
  const formatActivityDate = (date: Date) => {
    let day = date.getDate();
    let year = date.getFullYear();
    let fullDate: string = (day < 10 ? "0" + day : day) + "/12/" + year;
    return fullDate;
  };

  const [todayDate, setTodayDate] = useState<string>(
    formatActivityDate(new Date())
  );
  // fireworks when succeeded
  const particlesInit = useCallback(async (engine: Engine) => {
    await loadFull(engine);
  }, []);

  const particlesLoaded = useCallback(
    async (container: Container | undefined) => {
      await console.log(container);
    },
    []
  );

  useEffect(() => {
    const datesCopy = [...Dates];
    datesCopy.map((dateInfo, index) => {
      console.log(todayDate);
      console.log(dateInfo.date);
      console.log(dateInfo.date === todayDate);
      if (dateInfo.date === todayDate) {
        setDateInfo(dateInfo);
      }
    });
  }, [todayDate, dateInfos]);

  const handleOnSucces = () => {
    setSuccess(true);

    setTimeout(() => {
      router.push("/success/day");
    }, 3000);
  };
  return (
    <div className={styles.dayContainer + " " + todayDate}>
      <div className={styles.dayTop__box}>
        <h1 className={styles.dayTop__title}>Calendar puzzle</h1>
      </div>
      <div className={styles.dayBottom__box}>
        <Image
          src={swag}
          width={400}
          alt="Swag picture"
          className={styles.dayBottom__swag}
        />

        <h1 className={styles.dayBottom__dayNumber}>
          {dateInfos && dateInfos.date}
        </h1>
      </div>
      <div className="w-full flex justify-center relative">
        <div className="grid grid-cols-3 grid-rows-3 absolute top-0 left-1/2 transform -translate-x-1/2 w-[600px] h-full z-10 bg-[#181E14]">
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
          <div className="w-[200px] min-h-1/3 border"></div>
        </div>
        {dateInfos && (
          <JigsawPuzzle
            imageSrc={dateInfos.imageUrl}
            rows={3}
            columns={3}
            onSolved={handleOnSucces}
          />
        )}
      </div>
      {success && <h2 className={styles.successMessage}>Yaaaaasssss</h2>}
      {success && (
        <Particles
          id="tsparticles"
          init={particlesInit}
          loaded={particlesLoaded}
          options={{
            fullScreen: {
              zIndex: 1,
            },
            emitters: {
              position: {
                x: 50,
                y: 100,
              },
              rate: {
                quantity: 5,
                delay: 0.15,
              },
            },
            particles: {
              color: {
                value: ["#1E00FF", "#FF0061", "#E1FF00", "#00FF9E"],
              },
              move: {
                decay: 0.05,
                direction: "top",
                enable: true,
                gravity: {
                  enable: true,
                },
                outModes: {
                  top: "none",
                  default: "destroy",
                },
                speed: {
                  min: 50,
                  max: 100,
                },
              },
              number: {
                value: 0,
              },
              opacity: {
                value: 1,
              },
              rotate: {
                value: {
                  min: 0,
                  max: 360,
                },
                direction: "random",
                animation: {
                  enable: true,
                  speed: 30,
                },
              },
              tilt: {
                direction: "random",
                enable: true,
                value: {
                  min: 0,
                  max: 360,
                },
                animation: {
                  enable: true,
                  speed: 30,
                },
              },
              size: {
                value: 3,
                animation: {
                  enable: true,
                  startValue: "min",
                  count: 1,
                  speed: 16,
                  sync: true,
                },
              },
              roll: {
                darken: {
                  enable: true,
                  value: 25,
                },
                enlighten: {
                  enable: true,
                  value: 25,
                },
                enable: true,
                speed: {
                  min: 5,
                  max: 15,
                },
              },
              wobble: {
                distance: 30,
                enable: true,
                speed: {
                  min: -7,
                  max: 7,
                },
              },
              shape: {
                type: ["circle", "square"],
                options: {},
              },
            },
            responsive: [
              {
                maxWidth: 1024,
                options: {
                  particles: {
                    move: {
                      speed: {
                        min: 33,
                        max: 66,
                      },
                    },
                  },
                },
              },
            ],
          }}
        />
      )}
      {/* { Object.values(days).map((image, index) => (
                    <ImageCase 
                        key={index} 
                        imageNumber={image.src} 
                        puzzle={image.puzzle}
                        solution={image.solution}
                    />
                ))} */}
    </div>
  );
};


export default Day;