import { Button } from "../../components/utilities/button";

const Success = () => {
  return (
    <div className="min-h-screen w-full flex flex-col justify-center items-center">
      <h2 className="text-5xl mb-10">Bravo!</h2>
      <p className="mb-10">Revenez demain pour une autre image 😊</p>
      <Button text="Revenir au début" href="/" />
    </div>
  );
};

export default Success;
