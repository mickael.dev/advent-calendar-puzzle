import Link from "next/link"
import styles from '../styles/Home.module.css'
import DateCalculation from "../components/DateCalculation"
import Snow from "./Snow"
import { useEffect, useState } from "react";
import { useRouter } from 'next/router'

const Home = () => {
  const [todayDate, setTodayDate] = useState<Date>(new Date());
  const [countDown, setCountDown] = useState<number>(todayDate.getTime());
  let router = useRouter();

  setTimeout(() => {
    setTodayDate(new Date());
  }, 1000);

  useEffect(() => {
    let timeInMs: number =
      new Date(`${new Date().getFullYear()}/12/1 0:00:00`).getTime() -
      todayDate.getTime();
    // console.log(timeInMs);
    setCountDown(timeInMs);
    if (countDown < 0) {
      router.push("/calendar/day");
    }
  }, [todayDate, countDown, router]);

  return (
    <div className={styles.adventCalendar}>
      <Snow />
      <div className={styles.adventCalendar__titleBox}>
        <h1 className={styles.adventCalendar__title}>Advent calendar puzzle</h1>
      </div>
      <div className={styles.adventCalendar__dates}>
        <p>Puzzle starting in...</p>
        <DateCalculation countDown={countDown} />
      </div>
      <div className={styles.adventCalendar__nameBox}>By MG</div>
    </div>
  );
};


export default Home;